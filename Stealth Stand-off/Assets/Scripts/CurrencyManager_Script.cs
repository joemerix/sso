﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrencyManager_Script : MonoBehaviour
{
    public GameManager_Script gm;
    public GameObject currency;
    public GameObject currencyManagerCanvas;
    public Text currencyText;

    public int startingCurrencyValue = 0;
    public int currencyValue;
    public int newValue;

    public int endTurnIncrementationValue = 3;
    public int endLevelIncrementationValue = 5;

    // Start is called before the first frame update
    void Start()
    {
        currencyManagerCanvas = GameObject.Find("CurrencyManagerCanvas");
        int maxNumberOfCurrencyManagers = FindObjectsOfType<CurrencyManager_Script>().Length;
        gm = GameObject.Find("GameManager").GetComponent<GameManager_Script>();


        currencyText = currency.GetComponentInChildren<Text>();

        if (maxNumberOfCurrencyManagers < 1 || maxNumberOfCurrencyManagers > 1)
        {
            Destroy(currencyManagerCanvas);
        }

        else
        {
            DontDestroyOnLoad(currencyManagerCanvas);

        }
        currencyText.text = "Currency: " + startingCurrencyValue;


    }

    // Update is called once per frame
    void Update()
    {

    }

    public void EndTurnScoreUpdate()
    {
        currencyValue += endTurnIncrementationValue;
        currencyText.text = "Currency: " + currencyValue;
    }

    public void EndLevelScoreUpdate()
    {
        currencyValue += endLevelIncrementationValue;
        currencyText.text = "Currency: " + currencyValue;
    }
}
