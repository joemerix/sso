﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinDisplay_Script : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NextLevel()
    {
        GameObject gameManager = GameObject.Find("GameManager");
        int currentLevel = gameManager.GetComponent<GameManager_Script>().currentLevel;
        int nextLevel = currentLevel + 1;
        SceneManager.LoadScene("Level_" + nextLevel);
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene("Main_Menu");
    }

    public void Quit()
    {
        Debug.Log("Quit");
        Application.Quit();

    }
}