﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridPosition_Script : MonoBehaviour
{
    [SerializeField] private bool isPlayerHere;
    public GameObject player;
    public List<GameObject> adjacentGridPos;
    private GameObject mesh;
    [SerializeField] private Color okColour;
    private Color ogColour;
    public GameObject gameManager;

    #region ProbesVariables

    public GameObject[] probesOne;
    public GameObject[] probesTwo;
    public GameObject[] probesThree;
    public GameObject[] probesFour;
    public GameObject[] probesFive;

    public List<GameObject> possiblePlaces;
    public int limit;

    #endregion
    public void Start()
    {
      // We find the "Character_Prefab" and assign it to player GameObject
      // We assign mesh GameObject to the first child object of this GameObject (which is the mesh)
      // We assign ogColour to the GameObject's start colour (which we've pre-set in the Inspector)
      // We Invoke the GridSetup Method with a 0.01f second delay
      // We define the GameManager object as GM
      // We access gridPositions list in the GameManager and add each object with GridPosition script attached to the gridPositions list
        player = GameObject.Find("Character_Prefab");
        mesh = transform.GetChild(0).gameObject;
        ogColour = mesh.GetComponent<Renderer>().material.color;
        //GridSetup();
        Invoke("GridSetup", 0.01f);
        gameManager = GameObject.Find("GameManager");
        gameManager.GetComponent<GameManager_Script>().gridPositions.Add(gameObject);
    }


    public void GridSetup()
    {
        // We clear the possiblePlaces so that possiblePlaces size doesn't increase by the limit everytime it's called
        possiblePlaces.Clear();

        if (limit ==0)
        {
            return;
        }
        //We go through the loop which says if there is a stored grid we add it to possiblePlaces list
        for (int i=0; i< probesOne.Length; i++)
        {
            if (probesOne[i].GetComponent<Probes_Script>().storedGrid != null)
                possiblePlaces.Add(probesOne[i].GetComponent<Probes_Script>().storedGrid);

        }

        // if our limit is hit we stop and return
        if (limit ==1)
        {
            return;
        }

        //We go through the loop which says if there is a stored grid we add it to possiblePlaces list

        for (int i = 0; i < probesTwo.Length; i++)
        {
            if (probesTwo[i].GetComponent<Probes_Script>().storedGrid != null)
                possiblePlaces.Add(probesTwo[i].GetComponent<Probes_Script>().storedGrid);
      
        }

        // if our limit is hit we stop and return

        if (limit == 2)
        {
            return;
        }

        //We go through the loop which says if there is a stored grid we add it to possiblePlaces list

        for (int i = 0; i < probesThree.Length; i++)
        {
            if (probesTwo[i].GetComponent<Probes_Script>().storedGrid != null)
                possiblePlaces.Add(probesThree[i].GetComponent<Probes_Script>().storedGrid);

        }

        // if our limit is hit we stop and return

        if (limit == 3)
        {
            return;
        }

        //We go through the loop which says if there is a stored grid we add it to possiblePlaces list

        for (int i = 0; i < probesFour.Length; i++)
        {
            if (probesThree[i].GetComponent<Probes_Script>().storedGrid != null)
                possiblePlaces.Add(probesFour[i].GetComponent<Probes_Script>().storedGrid);

        }

        // if our limit is hit we stop and return

        if (limit == 4)
        {
            return;
        }

        //We go through the loop which says if there is a stored grid we add it to possiblePlaces list

        for (int i = 0; i < probesFive.Length; i++)
        {
            if (probesFour[i].GetComponent<Probes_Script>().storedGrid != null)
                possiblePlaces.Add(probesFive[i].GetComponent<Probes_Script>().storedGrid);

        }

        // if our limit is hit we stop and return

        if (limit == 5)
        {
            return;
        }

        //We go through the loop which says if there is a stored grid we add it to possiblePlaces list

    }

    #region MouseMethods
    public void OnMouseOver()
    {

        if (gameManager.GetComponent<GameManager_Script>().enemyPhase)
        {
            return;
        }
        // if there is a target We make a new list of GameObjects called newAdjacentPos. We add to that list the possible places for each element on that list
        
        if (player.GetComponent<playerScript>().target != null)
        {
            List<GameObject> newAdjacentPos = new List<GameObject>();
            newAdjacentPos.AddRange(player.GetComponent<playerScript>().target.GetComponent<GridPosition_Script>().possiblePlaces);

            //If our newAdjacentPos contains a gameObject we give it the okColour material. If the player puts his mouse over that object it'll go green
            if (newAdjacentPos.Contains(gameObject))
            {
                mesh.GetComponent<Renderer>().material.color = okColour;

                //We pass in Movement from the playerScript so that when we click the left mouse button we move to the position of the newTarget
                if (Input.GetMouseButtonUp(0))
                {
                    if (gameManager.GetComponent<GameManager_Script>().playPhase == false)
                        return;
                    GameObject clickCard = gameManager.GetComponent<GameManager_Script>().clickedCard;
                    gameManager.GetComponent<HandManager_Script>().playableCard.Remove(clickCard);
                    Destroy(clickCard);
                    clickCard = null;
                    gameManager.GetComponent<GameManager_Script>().SetLimit(0);
                    ResetMaterial();
                    player.GetComponent<playerScript>().Movement(transform);
                    gameManager.GetComponent<HandManager_Script>().CheckLeft();
                    gameManager.GetComponent<GameManager_Script>().EndPhase();
     
                }
            }
        }
    }
    //When we move our mouse off the object with this script attached it turns back to the default colour (ogColour yellow)
    public void OnMouseExit()
    {
        ResetMaterial();
    }

    public void ResetMaterial()
    {
        mesh.GetComponent<Renderer>().material.color = ogColour;

    }

    #endregion
}