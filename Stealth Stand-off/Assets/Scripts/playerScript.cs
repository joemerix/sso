﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class playerScript : MonoBehaviour
{
    public Transform currentPos;
    public bool live;
    [SerializeField] private float speed = 5f;
    public Transform target;
    public GameObject gameManager;
    public GameManager_Script gm_Script;
    public CurrencyManager_Script cm;

    private void Start()
    {
        cm= GameObject.Find("CurrencyManager").GetComponent<CurrencyManager_Script>();
        gameManager = GameObject.Find("GameManager");
        gameManager.GetComponent<GameManager_Script>().player = gameObject;
        gm_Script = GameObject.Find("GameManager").GetComponent<GameManager_Script>();
    }

    public void Update()
    {
        //If the game is live (true) then we move the player between it's current position and target position by "speed" over time

        if (!live)
            return;
        transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);        
    }

    public void Movement(Transform newTarget)
    {
        // We pass through the position of the newTarget so that when movement is called it contains the Transform of the newTarget
        //target is the specific grid we're moving too. That changes dynamically depending on our position
        target = newTarget;
    }

    public void OnTriggerEnter(Collider other)
    {

        if (other.name == "Win_GridPosition")
        {
            Invoke("Win", 2f);
            Debug.Log("Win");
        }
        if (other.name =="Enemy_Prefab")
        {
            gm_Script.InvokeGameOver();
        }
    }

    public void Win()
    {
        Time.timeScale = 0;
        gameManager = GameObject.Find("GameManager");
        GameObject winMenu = gameManager.GetComponent<GameManager_Script>().winMenu;
        cm.EndLevelScoreUpdate();
        winMenu.SetActive(true);    
    }
}