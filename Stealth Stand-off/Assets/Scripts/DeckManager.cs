﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckManager : MonoBehaviour
{
    public List<GameObject> originalDeck;
    public int prefabCardIndex;
    public GameObject[] movementPrefab;
    public List<GameObject> tempDeck;

    void Start()
    {
        int maxNumberOfDeckManagers = FindObjectsOfType<DeckManager>().Length;

        if (maxNumberOfDeckManagers < 1 || maxNumberOfDeckManagers > 1)
        {
            Destroy(gameObject);
        }

        else
        {
            DontDestroyOnLoad(gameObject);
        }

        for (int i = 0; i <= originalDeck.Count; i++)
        {
            prefabCardIndex = Random.Range(0, movementPrefab.Length);

            originalDeck.Add(movementPrefab[prefabCardIndex]);

            if (originalDeck.Count >= 30)
            {
             //   tempDeck.AddRange(originalDeck);
               return;
            }
        }
    }
}