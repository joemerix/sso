﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager_Script : MonoBehaviour
{
    [Header("Test shortcuts")]
    [SerializeField] string cardDraw = "D Key";
    [SerializeField] string inventoryScreen = "I Key";
    [SerializeField] string level01 = "0 Key";
    [SerializeField] string mainMenu = "M Key";

    public GameObject player;
    public GameObject[] unspawnedEnemies;
    public List<GameObject> enemies;
    public List<GameObject> gridPositions;
    public List<Transform> spawnPoints;
    List<Transform> usedSpawn;
    [SerializeField] private Transform startPos;
    public GameObject clickedCard;
    public bool drawPhase = false;
    public bool playPhase = false;
    public bool endPhase = false;
    public bool enemyPhase = false;
    public HandManager_Script handManagerScript;
    public PlayableCard_Script playableCardScript;
    public playerScript playerScript;
    public GameObject winMenu;
    [SerializeField] public int currentLevel;
    public float currentTime;
    CurrencyManager_Script cm;

    [Header("Text")]
    public Text turnText;

    [Header("Testing")]
    [SerializeField] private GameObject deckManager;

    
    void Awake()
    { 
        deckManager = GameObject.Find("DeckManager");
        if (deckManager == null)
            SceneManager.LoadScene("Main_Menu");

        //int maxNumberOfGameManagers = FindObjectsOfType<GameManager_Script>().Length;

        //if (maxNumberOfGameManagers < 1 || maxNumberOfGameManagers > 1)
        //{
        //    Destroy(this.gameObject);
        //}

        //else
        //{
        //    DontDestroyOnLoad(this.gameObject);
        //}
    }
    void Start()
    {
        cm= GameObject.Find("CurrencyManager").GetComponent<CurrencyManager_Script>();
        Debug.Log("We start");
        Time.timeScale = 1;
        winMenu = GameObject.Find("WinDisplay_Prefab");
        if(winMenu!=null)
        winMenu.SetActive(false);
        handManagerScript = GetComponent<HandManager_Script>();

        //We find the Character prefab, assign it as the player Gameobject
        player = GameObject.Find("Character_Prefab");

        //We access its playerScript and set its target (where it goes) to startPos
        player.GetComponent<playerScript>().target = startPos;

        turnText = GameObject.Find("TurnText").GetComponent<Text>();

        Invoke("DrawPhase", 0.2f);
        Invoke("EnemySpawn", 0.3f);

  
    }

    void Update()
    {
        //When we press the corresponding number keys we change the limit by that amount

        if (playPhase)
        {
            currentTime += 1 * Time.time;
        }

        if (Input.GetKeyDown(KeyCode.S) && enemyPhase == true)
        {
            DrawPhase();
        }

        if (Input.GetKeyDown(KeyCode.I))
        {
            SceneManager.LoadScene("Inventory");
        }

        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            SceneManager.LoadScene("Level_0");
        }

        if (Input.GetKeyDown(KeyCode.M))
        {
            SceneManager.LoadScene("Main_Menu");
        }
    }

    // We're passing through limit of type int
    public void SetLimit(int limit)
    {
        //We loop through the for loop until i !< gridPositions
        //Until then, we set limit (in GridPosition_Script) to the limit we've defined here (with the number keys)
        //Until then, we're also calling GridSetup(); which clears grid, assigns probes and possible places etc.
            //currentLevel = limit;
        for (int i=0; i<gridPositions.Count; i++)
        {
            gridPositions[i].GetComponent<GridPosition_Script>().limit = limit;
            gridPositions[i].GetComponent<GridPosition_Script>().GridSetup();
            
        }
    }
    
    //Optional InvokeGameOver gives us a chance to tell the user why they've lost
    public void InvokeGameOver()
    {
        Invoke("GameOver", 2f);
        Debug.Log("Invoke Called");
    }

    public void GameOver()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Debug.LogWarning("Game Over");
    }

    public void TurnLogic()
    {
   
    }

    #region

    public void DelayedDraw()
    {
        SetLimit(0);
        Invoke("DrawPhase", 3f);
    }
    public void DrawPhase()
    {
        Debug.Log("Drawphase");
        drawPhase = true;
        enemyPhase = false;
        turnText.text = "Draw Phase";

        if (handManagerScript.currentDeck.Count >=1 && drawPhase && handManagerScript.playableCard.Count <=4)
        {
            Debug.Log("should be under");
            handManagerScript.DrawCard();
            drawPhase = false;
        }

        if (handManagerScript.currentDeck.Count >= 1 && drawPhase && handManagerScript.playableCard.Count >5)
        {
            Debug.Log("herre we are");
            drawPhase = false;
        }

        if (handManagerScript.currentDeck.Count == 0)
        {
            InvokeGameOver();
        }
        Invoke("PlayPhase", 1f);
    }

    public void PlayPhase()
    {
        turnText.text = "";
        drawPhase = false;
        playPhase = true;
        turnText.text = "Play Phase";
       playPhase = true;

        currentTime = 0f;


        float turnTimeLimit = 30f;

        if (currentTime >= turnTimeLimit)
        {
            playPhase = false;
            EndPhase();
            Debug.Log("Timeout");
           
        }
       
        //Needs to be extended to make sure that after a user plays a card we move to the end phase
    }

    public void EndPhase()
    {
        playPhase = false;
        turnText.text = "End Phase";
        endPhase = true;
        cm.EndTurnScoreUpdate();
        Invoke("ClearText", 2.3f);
        Invoke("EnemyPhase", 3f);
        

        // Card we play during play phase now activates

       //Set Playable Card value to 0
        // Needs to be extended to make sure the user cannot click on any cards
    }

    public void EnemyPhase()
    {
        turnText.text = "Enemy Phase";
        enemyPhase = true;
        if (enemies == null)
        {
            DrawPhase();
            return;
        }

        for (int i=0; i<enemies.Count; i++)
        {
            enemies[i].GetComponent<Enemy_Script>().SelectAction();
        }

    }


    public void ClearText()
    {
        turnText.text = "";
        endPhase = false;
    }

    #endregion

    #region EnemyAction

    public void EnemySpawn()
    {
        enemies[0].GetComponent<Enemy_Script>().prime = true;

        for (int i=0; i< enemies.Count; i++)
        {
           int ran = Random.Range(0, spawnPoints.Count);
            enemies[i].GetComponent<Enemy_Script>().currentLocation = spawnPoints[i];
           enemies[i].GetComponent<Enemy_Script>().target = spawnPoints[i].gameObject;
           enemies[i].transform.position = spawnPoints[i].transform.position;
        }
    }
    #endregion
}
