﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy_Script : MonoBehaviour
{
    public float speed = 5f;
    public GameObject target;
    public Transform currentLocation;
    public GameObject gameManager;
    public List<GameObject> potentialMoves;
    public bool prime;

    // Start is called before the first frame update
    public void Start()
    {
        gameManager = GameObject.Find("GameManager");
        gameManager.GetComponent<GameManager_Script>().enemies.Add(gameObject);
        //if (gameManager == null)
        //{
        //    return;
        //}
        
    }
    public void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);
    }
    public void SelectAction()
    {
        Move(Random.Range(1, 3));
    }

    public void Move(int length)
    {
        gameManager.GetComponent<GameManager_Script>().SetLimit(length);
        potentialMoves.AddRange(currentLocation.GetComponent<GridPosition_Script>().possiblePlaces);
        target = potentialMoves[Random.Range(0, potentialMoves.Count)];
        if (prime)
        {
            gameManager.GetComponent<GameManager_Script>().DelayedDraw();
        }
    }
}