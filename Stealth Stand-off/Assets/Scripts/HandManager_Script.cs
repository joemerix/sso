﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandManager_Script : MonoBehaviour
{
    [Header("Deck")]
    public List<GameObject> originalDeck;
    public List<GameObject> tempDeck;
    public List<GameObject> currentDeck;
    public Transform deckVisual;
    public GameObject[] movementPrefab;

    [Header("Hand")]
    public List<GameObject> hand;
    
    public List<GameObject> playableCard;
    public GameObject playableCardPrefab;
    public Transform handStart; //The right most place in the game. Gives the pefab somewhere to move when instantiated

    public GameManager_Script gameManager;
    public DeckManager deckManager;

    public GameObject hovered;

    public int prefabCardIndex;

    public bool handFull;

    void Start()
    {
        gameManager = GetComponentInChildren<GameManager_Script>();
        deckManager = GameObject.Find("DeckManager").GetComponent<DeckManager>();

        if (deckManager.originalDeck.Count >= 30)
        {
            tempDeck.AddRange(deckManager.originalDeck);
        }
        ShuffleSingleCard();
        DrawCard();

    }
    void Update()
    {

        // Draw Card Here
        if (Input.GetKeyDown(KeyCode.D))
        {
            DrawCard();

        }

        if (Input.GetKeyDown(KeyCode.U))
        {
            deckManager.originalDeck.RemoveRange(0, originalDeck.Count);
        }


    }

    public void ShuffleSingleCard()
    {
        int cardNumber = Random.Range(0, tempDeck.Count);

        currentDeck.Add(tempDeck[cardNumber]);

        tempDeck.Remove(tempDeck[cardNumber]);

        if (tempDeck.Count > 0)

            ShuffleSingleCard();
    }

    public void DrawCard()
    {
        if (tempDeck.Count != 0)
        {
            currentDeck.AddRange(tempDeck);
            tempDeck.Clear();
        }

        if (currentDeck.Count <= 0)
        {
            gameObject.GetComponent<GameManager_Script>().InvokeGameOver();
            return;
        }
        Debug.Log("Draw card");

        if (deckVisual == null)
            deckVisual = GameObject.Find("Deck_Visual").transform;
        GameObject newCard = Instantiate(playableCardPrefab, deckVisual.position, deckVisual.rotation);

        if (handStart.GetComponent<CardPosition_Script>().myCard == null)
        {
            //This is always sending new card to the left most space of the hand.
            newCard.GetComponent<PlayableCard_Script>().target = handStart;
            handStart.GetComponent<CardPosition_Script>().myCard = newCard;


        }

        else if (handStart.GetComponent<CardPosition_Script>().myCard != null)
        {
            DetermineHandPos(handStart, newCard);
        }

        newCard.GetComponent<PlayableCard_Script>().moveValue = currentDeck[0].GetComponent<CardData_Script>().moveValue;

        hand.Add(currentDeck[0]);

        currentDeck.Remove(currentDeck[0]);


        if (currentDeck.Count <= 0)
        {
            deckVisual.gameObject.SetActive(false);
        }
        CheckLeft();

    }

    void DetermineHandPos(Transform currentCheck, GameObject newCard)
    {
        if (currentCheck.GetComponent<CardPosition_Script>().myCard == null)
        {
            newCard.GetComponent<PlayableCard_Script>().target = currentCheck;
            currentCheck.GetComponent<CardPosition_Script>().myCard = newCard;
        }

        else
        {
            if (currentCheck.GetComponent<CardPosition_Script>().myRight != null)
            {
                Transform newCheck;
                newCheck = currentCheck.GetComponent<CardPosition_Script>().myRight;
                DetermineHandPos(newCheck, newCard);
            }
        }
    }

    public void CheckLeft()
    {
        for (int i = 0; i < playableCard.Count; i++)
        {
            playableCard[i].GetComponent<PlayableCard_Script>().CheckLeft();
        }
    }

    public void SetLayer(int layer)
    {
        Debug.Log("Set Layer Is Called");
        for (int i = 0; i < playableCard.Count; i++)
        {
            playableCard[i].layer = layer;
            Debug.Log(layer.ToString());
        }
    }
}