﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardPosition_Script : MonoBehaviour
{
    public Transform myLeft;
    public Transform myRight;

    public Transform mouseOver;

    public bool leftist;
    public GameObject gameManager;
    public GameObject myCard;

    void Start()
    {
        gameManager = GameObject.Find("GameManager");
    }

    void Update()
    {
        if (leftist)
        {
            if (gameManager.GetComponent<HandManager_Script>().hand.Count >0)
            {
                gameManager.GetComponent<HandManager_Script>().CheckLeft();
            }
        }
    }
}
