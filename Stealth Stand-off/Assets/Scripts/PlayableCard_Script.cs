﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayableCard_Script : MonoBehaviour
{
    public Transform target;
    public bool moving;
    public GameObject gameManager;
    public float speed = 5f;
    public bool mouseOver = false;
    public bool clicked = false;
    public Transform mouseOverPosition;

    public TextMesh moveValueText;
    [Header("Attributes")]
    public int moveValue;
    public Vector3 mousePos;
   // public Camera orthographic;
    public float posPastHand = -12f;
    public float maxSize = 1f;
    public float scaleSpeed = 0.4f;
    public bool shrink = false;
    public bool scale = false;

    public Vector3 tempVect;
    public Vector3 defaultScaleSize;

    void Start()
    {
        defaultScaleSize = new Vector3(1f, 1f, 1f);
        moveValueText.text = moveValue.ToString();
        gameManager = GameObject.Find("GameManager");
        moving = true;
        gameManager.GetComponent<HandManager_Script>().playableCard.Add(gameObject);
       // orthographic = gameManager.GetComponent<GameManager_Script>().orthographic;
    }

    void Update()
    {
        if (moving)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }

        if (mouseOver && !clicked)

        {
            transform.position = Vector3.MoveTowards(transform.position, mouseOverPosition.position, speed * Time.deltaTime);

            if (Input.GetMouseButton(0))
            {
                clicked = true;
                gameManager.GetComponent<GameManager_Script>().clickedCard = gameObject;
            }

       }

        if (clicked)
        {
            Drag();
            gameManager.GetComponent<HandManager_Script>().SetLayer(2);
        }

        if (!clicked)
        {
            shrink = false;
            gameObject.layer = 0;
        }

        if (Input.GetMouseButtonUp(0))
        {
            clicked = false;
            gameManager.GetComponent<GameManager_Script>().clickedCard = null;
            gameManager.GetComponent<GameManager_Script>().SetLimit(0);
        }

        if (shrink)
        {
            Scale(false);
        }

        if (!shrink)
        {
            Scale(true);
        }
    }

    public void CheckLeft()
    {
        Transform myLeft = target.GetComponent<CardPosition_Script>().myLeft;
        
        if (myLeft != null && myLeft.GetComponent<CardPosition_Script>().myCard == null)
        {
            target.GetComponent<CardPosition_Script>().myCard = null;
            Transform newTarget = target.GetComponent<CardPosition_Script>().myLeft;
            target = newTarget;
            target.GetComponent<CardPosition_Script>().myCard = gameObject; 
        } 
    }

    public void OnMouseOver()
    {
        if (gameManager.GetComponent<GameManager_Script>().clickedCard != null)
        {
            return;
        }
        //if (locked)
        //    return;

      //if (gameManager.GetComponent<HandManager_Script>().hovered !=null)
      //  {
      //      return;
      //  }
      //  gameManager.GetComponent<HandManager_Script>().hovered = gameObject;
        mouseOver = true;
        SetMouseOverPosition();
      //  gameManager.GetComponent<HandManager_Script>().LockHand(gameObject);
    }

    public void SetMouseOverPosition()
    {
        mouseOverPosition = target.GetComponent<CardPosition_Script>().mouseOver;
    }

    public void OnMouseExit()
    {
        mouseOver = false;
        //If we let go of the mouse while dragging the card it becomes the default size UNLESS we extend this code to check if the move is valid
        transform.localScale = defaultScaleSize;

        if (!clicked)
        {
            gameManager.GetComponent<HandManager_Script>().hovered = null;
            
        }

    }

    private void Drag()
    {
        clicked = true;

        gameManager.GetComponent<GameManager_Script>().SetLimit(moveValue);

        //mousePos = orthographic.ScreenToWorldPoint(Input.mousePosition);
        // mousePos.y = 13f;
        // transform.position = mousePos;

        mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z + 47f));
        transform.position = mousePos;

        //Create a dodgem like system controlling Positions

        if (transform.position.z >= posPastHand)
        {
            shrink = true;

            if (tempVect.x <= 0.4120f && tempVect.y <= 0.4120f && tempVect.z <= 0.4120f)
            {
                //We stop shrinking when one temp vector scale sizes hits the target size
                shrink = false;
            }
        }

        if (transform.position.z <= posPastHand)
        {
            shrink = false;

            //We go back to the normal size if we return the card to the hand position
            transform.localScale = defaultScaleSize;
        }
    }

     public void Scale(bool up)
     {
     
     tempVect = transform.localScale;

        if (!up)
        {
            tempVect.x -= scaleSpeed * Time.deltaTime;
            tempVect.y -= scaleSpeed * Time.deltaTime;
            tempVect.z -= scaleSpeed * Time.deltaTime;

            transform.localScale = new Vector3(tempVect.x, tempVect.y, tempVect.z);
        }
      
        //Put a bool down condition in case the user releases the card without placing it on a gridPosition (or an incorrect position)
       
    }


}
