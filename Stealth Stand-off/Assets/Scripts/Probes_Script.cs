﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Probes_Script : MonoBehaviour
{
    public GameObject storedGrid;
    // Start is called before the first frame update
   
    // Because the probes are colliding with the grid gameobject on top of it we store that object as storedGrid for player movement positions
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.transform.tag == "GridPosition")
        {
            
            storedGrid = collision.gameObject;
            
        }
    }
}
