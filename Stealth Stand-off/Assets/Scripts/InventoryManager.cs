﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InventoryManager : MonoBehaviour
{
    public List<GameObject> inventoryPos;
    public GameObject playableCardPrefab;
    public DeckManager dm;

    public int prefabCardIndex;

    // Start is called before the first frame update
    void Start()
    {
        dm = GameObject.Find("DeckManager").GetComponent<DeckManager>();

        for (int i = 0; i < inventoryPos.Count; i++)
        {
           Vector3 inventoryPositions = new Vector3(inventoryPos[i].transform.position.x, inventoryPos[i].transform.position.y, inventoryPos[i].transform.position.z);

            playableCardPrefab.GetComponent<PlayableCard_Script>().moveValue = dm.originalDeck[i].GetComponent<CardData_Script>().moveValue;
            Instantiate(playableCardPrefab, inventoryPositions, Quaternion.identity);
            
            //instantiate the playable card and set its moveValue to the hm.movevalue
        }

    }

    // Update is called once per frame
    void Update()
    {

    }
}
